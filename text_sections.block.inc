<?php

/**
 * @file
 * Block output of the Text Sections field
 */

/**
 * Implments hook_block_info()
 */
function text_sections_block_info() {
  $blocks = array();

  text_sections_set_block_instances();
  $instances = text_sections_get_block_instances();
  foreach ($instances as $instance) {
    if($instance->enabled){
      // Pull this to get the label info
      $full_instance = field_info_instance($instance->entity_type, $instance->field_name, $instance->bundle);
      $blocks['text_sections_' . $instance->iid] = array(
        'info' => t('Text Sections: @field Block', array('@field' => $full_instance['label'])),
        'cache' => DRUPAL_CACHE_PER_PAGE,
      );
    }
  }

  return $blocks;
}

/**
 * Implements hook_block_view();
 */
function text_sections_block_view($delta = '') {
  drupal_add_css(drupal_get_path('module', 'text_sections') . '/css/text-sections-block.css');
  $block = array();
  // First we need to find our deltas
  if (strpos($delta, 'text_sections_') === 0) {
    // We'll strip off the prefix and we have our instance key
    $iid = intval(substr($delta, 14));
    // And with that we'll grab the instance info
    $instance = text_sections_get_block_instance($iid);
    // Hooray for obliquely passing data!

    // We're making some assumptions to do the render here
    $entity_type = arg(0);
    $entity_id = arg(1);
// TODO: Check these please!
    $entities = entity_load($entity_type, array($entity_id));

    $items = field_get_items($entity_type, $entities[$entity_id], $instance->field_name);
    
    // Call alter hooks to add items
    drupal_alter('text_sections', $items, $entities[$entity_id]);

    if (!empty($items)) {
      $full_instance = field_info_instance($instance->entity_type, $instance->field_name, $instance->bundle);
      $block['subject'] = $full_instance['label'];
      $block['content'] = array(
        '#theme' => 'item_list',
        '#type' => 'ul',
        '#items' => text_sections_make_tree($items),
      );
    }
  }
  return $block;
}

/**
 * Implements hook_preprocess_block();
 */
function text_sections_preprocess_block(&$variables) {
  // Eventually we'll need a setting and to sort through which deltas, but for now just add to all
  if ($variables['block']->module == 'text_sections' /*&& $variables['block']->delta == 'contact_block'*/) {
    $variables['classes_array'][] = drupal_html_class('block-text-sections-expandable');
  }
}

/**
 * Refreshes the text section instance table.
 */
function text_sections_set_block_instances() {
  // Internal list of available widgets
  $widgets = array('text_sections_field_widget');
  // We'll start by looking for all the fields that need blocks
  $instances = field_info_instances();

  $instance_list = array();

  foreach ($instances as $entity_type => $bundles) {
    foreach ($bundles as $bundle => $fields) {
      foreach ($fields as $field_name => $field) {
        if (isset($field['widget']) && isset($field['widget']['type']) && in_array($field['widget']['type'], $widgets)) {
          $result = db_query("SELECT i.iid, i.enabled FROM {text_sections_instance_blocks} i WHERE entity_type = :entity AND bundle = :bundle AND field_name = :field", array(':entity' => $entity_type, ':bundle' => $bundle, ':field' => $field_name));
          if ($result->rowCount() == 0) {
            db_insert('text_sections_instance_blocks')
              ->fields(array(
                'entity_type' => $entity_type,
                'bundle' => $bundle,
                'field_name' => $field_name,
                'enabled' => intval($field['settings']['expose_block']),
              ))->execute();
          }
          else {
            // TODO: Check also if field has been deleted
            $record = $result->fetchObject();
            if ($record->enabled != intval($field['settings']['expose_block']))
              db_update('text_sections_instance_blocks')
                ->condition('iid', $record->iid)
                ->fields(array('enabled' => intval($field['settings']['expose_block']), ))
                ->execute();
          }
        }
      }
    }
  }
}

/**
 * Returns all instances of text section blocks
 */
function text_sections_get_block_instances() {
  $result = db_query("SELECT i.iid, i.entity_type, i.bundle, i.field_name, i.enabled FROM {text_sections_instance_blocks} i");
  return $result->fetchAll();
}

/**
 * Returns a specific instance of a text section block
 */
function text_sections_get_block_instance($iid) {
  $result = db_query("SELECT i.iid, i.entity_type, i.bundle, i.field_name, i.enabled FROM {text_sections_instance_blocks} i WHERE iid = :iid AND enabled = :enabled", array(':iid' => intval($iid), ':enabled' => 1, ));
  return $result->fetchObject();
}