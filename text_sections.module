<?php

/**
 * @file
 * Defines text sections field type
 */

define('TEXT_SECTIONS_NONE', 0);
define('TEXT_SECTIONS_MANUAL', 1);
define('TEXT_SECTIONS_SEMI', 2);
define('TEXT_SECTIONS_AUTO', 3);
define('TEXT_SECTIONS_FIELD_TYPE', 'text_sections_field');

include('text_sections.block.inc');
include('text_sections.links.inc');

/**
 * Implements hook_field_info().
 */
function text_sections_field_info() {
  return array(
    TEXT_SECTIONS_FIELD_TYPE => array(
      'label' => t('Text Sections'),
      'description' => t('Store section headlines and anchors associated with a long text field for use in rendering to navigate a document.'),
      'instance_settings' => array(
        'expose_block' => '',
        'attach' => '',
      ),
      'default_widget' => 'text_sections_field_widget',
      'default_formatter' => 'text_sections_default',
    ),
  );
}

/**
 * Implements hook_field_widget_info().
 */
function text_sections_field_widget_info() {
  return array(
    'text_sections_field_widget' => array(
      'label' => t('Text Sections'),
      'field types' => array(TEXT_SECTIONS_FIELD_TYPE),
    ),
  );
}

/**
 * Implements hook_element_info().
 */
function text_sections_element_info() {
  $elements = array();
  $elements['text_sections_field'] = array(
    '#input' => TRUE,
    '#theme_wrappers' => array('form_element'),
  );
  return $elements;
}

/**
 * Implements hook_field_formatter_info().
 */
function text_sections_field_formatter_info() {
  return array(
    'text_sections_default' => array(
      'label' => t('Default'),
      'field types' => array(TEXT_SECTIONS_FIELD_TYPE),
    ),
  );
}

/**
 * Implements hook_field_widget_form().
 */
function text_sections_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $item =& $items[$delta];

  // This is the switch we're flipping in the custom multi form
  $element['#nested'] = TRUE;

  // Set the type
  $element['#type'] = TEXT_SECTIONS_FIELD_TYPE;

  $element['name'] = array(
    '#type' => 'textfield',
    '#size' => 60,
    '#title' => t('Name'),
    '#title_display' => 'invisible',
    '#default_value' => isset($item['name']) ? $item['name'] : NULL,
  );
  $element['anchor'] = array(
    '#type' => 'textfield',
    '#size' => 20,
    '#title' => t('Anchor'),
    '#title_display' => 'invisible',
    '#field_prefix' => '#',
    '#default_value' => isset($item['anchor']) ? $item['anchor'] : NULL,
  );
  $element['id'] = array(
    '#type' => 'textfield',
    '#size' => 3,
    '#default_value' => $delta,
  );
  $element['pid'] = array(
    '#type' => 'textfield',
    '#size' => 3,
    '#default_value' => isset($item['pid']) ? $item['pid'] : NULL,
  );
  return $element;
}

/**
 * Implements hook_field_formatter_view();
 */
function text_sections_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  // Currently we only have one display option, use switch
  // to prepare for future options.
  switch ($display['type']) {
    case 'text_sections_default':
      $element = array(
        '#theme' => 'item_list',
        '#items' => text_sections_make_tree($items),
        '#type' => 'ul',
      );
      break;
    default:
  }
  return $element;
}

/**
 * Recursively builds a tree of links suitable for themeing as an item list
 */
function text_sections_make_tree(array $elements, $parent_id = NULL) {
  $branch = array();
  foreach ($elements as $key => $element) {
    $link_element = array();
    if (strval($element['pid']) == strval($parent_id)) {
      $children = text_sections_make_tree($elements, $key);
      if ($children) {
        $link_element['children'] = $children;
      }
      $link_data = array(
        'fragment' => $element['anchor'],
        'external' => TRUE,
        'attributes' => array(
          'title' => $element['name'],
        ),
        'html' => TRUE,
      );
      $link_element['data'] = l($element['name'], '', $link_data);
      $branch[] = $link_element;
    }
  }
  return $branch;
}

/**
 * Implements hook_field_is_empty().
 */
function text_sections_field_is_empty($item, $field) {
  return empty($item['name']);
}

/**
 * Implements hook_field_validate().
 */
function text_sections_field_validate($obj_type, $object, $field, $instance, $langcode, &$items, &$errors) {
  foreach ($items as $delta => $item) {
    $name = trim($item['name']);
    $anchor = trim($item['anchor']);
    // Check for an empty anchor
    if (empty($name) && !empty($anchor)) {
      $errors[$field['field_name']][$langcode][$delta][] = array(
        'error' => 'missing_name',
        'message' => t('%name: missing name for anchor "#%anchor"', array('%name' => $instance['label'], '%anchor' => $anchor)),
      );
    // Check for an empty name
    }
    elseif (!empty($name) && empty($anchor)) {
      $errors[$field['field_name']][$langcode][$delta][] = array(
        'error' => 'missing_anchor',
        'message' => t('%name: missing an anchor for section "%section"', array('%name' => $instance['label'], '%section' => $name)),
      );
    }
  }
}

/**
 * Implements hook_field_widget_error();
 */
function text_sections_field_widget_error($element, $error, $form, &$form_state) {
  switch ($error['error']) {
    case 'missing_anchor':
      form_error($element['anchor'], $error['message']);
      break;
    case 'missing_name':
      form_error($element['name'], $error['message']);
      break;
  }
}

/**
 * Implements hook_field_instance_settings_form();
 */
function text_sections_field_instance_settings_form($field, $instance) {
  $settings = $instance['settings'];
  // Known widget types that this can work with.
  $available = array('text_textarea', 'text_textarea_with_summary');

  $form['expose_block'] = array(
    '#type' => 'checkbox',
    '#title' => t('Expose this field as a block.'),
    '#default_value' => isset($settings['expose_block']) ? $settings['expose_block'] : FALSE,
  );

  // Get all the instances for this content type
  $instances = field_info_instances($instance['entity_type'], $instance['bundle']);

  // Gather available fields that are options
  $options = array('' => '- None -');
  foreach ($instances as $machine_name => $field_instance) {
    if (in_array($field_instance['widget']['type'], $available))
      $options[$machine_name] = $field_instance['label'] . ' (' . $machine_name . ')';
  }
  $form['attach'] = array(
    '#type' => 'select',
    '#title' => t('Attach this field to a specific long text field'),
    '#description' => t('Attaching to a long text field allows for automated generation and placement of named anchors based on title structure.'),
    '#options' => $options,
    '#default_value' => isset($settings['attach']) ? $settings['attach'] : '',
  );

  return $form;
}

/**
 * Implements hook_theme_registry_alter().
 */
function text_sections_theme_registry_alter(&$theme_registry) {
  if (isset($theme_registry['field_multiple_value_form'])) {
    $theme_registry['field_multiple_value_form']['type'] = 'module';
    $theme_registry['field_multiple_value_form']['theme path'] = drupal_get_path('module', 'text_sections');
    $theme_registry['field_multiple_value_form']['function'] = 'text_sections_field_multiple_value_form';
  }
}

/**
 * Sorts items by parent/child/weight
 *
 * @see tabledrag_example_parent_get_data()
 */
function text_sections_start_tree(&$items) {
  $rootnodes = array();
  foreach ($items as $key => $item) {
    $items[$key]['#key'] = $key;
    $item['#key'] = $key;
    if (!isset($item['pid']) || $item['pid']['#value'] == NULL )
      $rootnodes[$key] = $item;
  }
  // Sorts by weight
  usort($rootnodes, '_field_sort_items_value_helper');
  $itemtree = array();
  $depth = -1;
  foreach ($rootnodes as $parent) {
    text_sections_build_tree($parent, $itemtree, $depth, $items);
  }
  $items = array_values($itemtree);
}

/**
 * Recursively adds to itemtree array, sorts by parent/child/weight
 *
 * @see tabledrag_example_get_tree()
 */
function text_sections_build_tree($parentitem, &$itemtree = array(), &$depth = 0, $items) {
  $depth++;
  $parentitem['#depth'] = $depth;
  $itemtree[$parentitem['#key']] = $parentitem;
  $children = array();
  foreach ($items as $key => $item) {
    // An ugly comparison, but deals with notorious 0 == "0" == "" nonsense
    if ($item['pid']['#value'] == strval($parentitem['id']['#value']))
      $children[$key] = $item;
  }
  // Sorts by weight
  usort($children, '_field_sort_items_value_helper');
  foreach ($children as $child) {
    // Make sure this child does not already exist in the tree, to avoid loops.
    if (!in_array($child['#key'], array_keys($itemtree))) {
      // Add this child's tree to the $itemtree array.
      text_sections_build_tree($child, $itemtree, $depth, $items);
    }
  }
  $depth--;
}

/**
 * Theme function override for multiple-value form widgets.
 *
 * This keeps default functionality but also reads for a #nested
 * option that allows for parent/child interactions
 *
 * @see theme_field_multiple_value_form()
 */
function text_sections_field_multiple_value_form($variables) {
  $element = $variables['element'];
  $output = '';
  $required = !empty($element['#required']) ? theme('form_required_marker', $variables) : '';

  // Looking at text section fields specifically
  if (isset($element[0]['#type']) && $element[0]['#type'] == TEXT_SECTIONS_FIELD_TYPE) {
    // Add in the field's CSS
    drupal_add_css(drupal_get_path('module', 'text_sections') . '/css/text-sections-admin.css');

    // So we're gonna build this form, and then JK NOT A FORM
    $sections_info_form = drupal_get_form('text_sections_field_entity_settings_form', $element);
    $sections_info_form = array_intersect_key($sections_info_form, array_flip(element_children($sections_info_form)));
    $output .= '<fieldset><legend>' . $element['#title'] . $required . '</legend>';
    unset($sections_info_form['form_build_id']);
    unset($sections_info_form['form_token']);
    unset($sections_info_form['form_id']);

    $output .= drupal_render($sections_info_form);
  }

  // Move into the actual fields
  if ($element['#cardinality'] > 1 || $element['#cardinality'] == FIELD_CARDINALITY_UNLIMITED) {
    $table_id = drupal_html_id($element['#field_name'] . '_values');
    $order_class = $element['#field_name'] . '-delta-order';
    if (isset($element[0]['#nested'])) {
      $nest_class = $element['#field_name'] . '-item-nesting';
      $reference_class = $element['#field_name'] . '-parent-reference';
      $id_class = $element['#field_name'] . '-reference-id';
    }

    $header = array(
      array(
        'data' => '<label>' . t('!title !required', array('!title' => $element['#title'], '!required' => $required)) . "</label>",
        'colspan' => 2,
        'class' => array('field-label'),
      ),
      t('Order'),
    );
    if (isset($element[0]['#nested'])) {
      $header[] = array(
        'data' => t('ID'),
        'class' => array('tabledrag-hide'), // Not sure why this isn't done as a part of tabledrag automatically, but whatever
      );
      $header[] = t('PID');
    }
    $rows = array();

    // Sort items according to '_weight' (needed when the form comes back after
    // preview or failed validation)
    $items = array();
    foreach (element_children($element) as $key) {
      if ($key === 'add_more') {
        $add_more_button = &$element[$key];
      }
      else {
        $items[] = &$element[$key];
      }
    }

    if (isset($element[0]['#nested'])) {
      text_sections_start_tree($items);
    }
    else {
      usort($items, '_field_sort_items_value_helper');
    }

    // Add the items as table rows.
    foreach ($items as $key => $item) {
      $item['_weight']['#attributes']['class'] = array($order_class);
      $delta_element = drupal_render($item['_weight']);
      if (isset($element[0]['#nested'])) {
        $item['id']['#attributes']['class'] = array($nest_class, $id_class);
        $id_element = drupal_render($item['id']);
        unset($item['id']);
        $item['pid']['#attributes']['class'] = array($nest_class, $reference_class);
        $pid_element = drupal_render($item['pid']);
        unset($item['pid']);
        unset($item['header']);
      }
      $indent = isset($item['#depth']) ? $item['#depth'] : 0;
      $indent = theme('indentation', array('size' => $indent));
      $cells = array(
        array(
          'data' => $indent,
          'class' => array('field-multiple-drag'),
        ),
        array(
          'data' => drupal_render($item),
          'class' => array('text-sections-fields'),
        ),
        array(
          'data' => $delta_element,
          'class' => array('delta-order'),
        ),
      );
      if (isset($element[0]['#nested'])) {
        $cells[] = array(
          'data' => $id_element,
          'class' => array('reference-id', 'tabledrag-hide'),
        );
        $cells[] = array(
          'data' => $pid_element,
          'class' => array('parent-reference'),
        );
      }
      $rows[] = array(
        'data' => $cells,
        'class' => array('draggable'),
      );
    }

    $output .= '<div class="form-item">';
    $output .= theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('id' => $table_id, 'class' => array('field-multiple-table'))));
    $output .= $element['#description'] ? '<div class="description">' . $element['#description'] . '</div>' : '';
    $output .= '<div class="clearfix">' . drupal_render($add_more_button) . '</div>';
    $output .= '</div>';

    drupal_add_tabledrag($table_id, 'order', 'sibling', $order_class);
    if (isset($element[0]['#nested'])) {
      drupal_add_tabledrag($table_id, 'match', 'parent', $nest_class, $reference_class, $id_class);
    }

    // We want to add states to the table, but it's not being built with the Form API so...
    drupal_add_library('system', 'drupal.states');
    // The following is a condensed version of drupal_process_states() and drupal_process_attached()
    $states_data = array(
      'states' => array(
        '#' . $table_id => array(
          'visible' => array(   // action to take.
            ':input[name="' . $element['#field_name'] . '[' . $element['#language'] . '][section_type]"]' => array('value' => TEXT_SECTIONS_MANUAL),
          ),
        ),
      ),
    );
    $states_options = array(
      'type' =>'setting',
      'group' => JS_DEFAULT,
      'every_page' => NULL,
    );
    drupal_add_js($states_data, $states_options);
  }
  else {
    foreach (element_children($element) as $key) {
      $output .= drupal_render($element[$key]);
    }
  }
  if (isset($element[0]['#type']) && $element[0]['#type'] == TEXT_SECTIONS_FIELD_TYPE) {
    $output .= '</fieldset>';
  }
  return $output;
}

/**
 * Implements hook_field_presave().
 */
function text_sections_field_presave($entity_type, $entity, $field, $instance, $langcode, &$items) {
  if ($field['type'] == 'text_sections_field') {

    // If this field has an attachment and is set to auto, we need to generate the sections here
    if ($instance['settings']['attach'] != '') {
      // Get the field settings
      $section_type = intval($_POST[$field['field_name']][$langcode]['section_type']);
      if ($section_type == TEXT_SECTIONS_AUTO) {
        $max_depth = intval($_POST[$field['field_name']][$langcode]['max_depth']);

        // Get the text with filters applied so there's no suprises there
        $text = check_markup($entity->{$instance['settings']['attach']}[$langcode][0]['value'], $entity->{$instance['settings']['attach']}[$langcode][0]['format'], $langcode); // TODO: Check before access here, maybe even log an error
        $items = text_sections_generate_sections($text, 'sections', $max_depth);
        return TRUE;
      }
    }

    // Loop through the values to fix the parent IDs
    foreach ($items as $key => $value) {
      if ($value['pid'] == '') {
        // Fix empty parent IDs to appear as NULL
        $value['pid'] = NULL;
      }
      else {
        // Fix actual parent IDs to point to the correct delta value
        $replace = FALSE;
        foreach ($items as $pkey => $pvalue) {
          if ($pvalue['id'] == $value['pid'])
            $replace = $pkey;
        }
        $value['pid'] = ($replace) ? $replace : NULL;
      }
      $items[$key] = $value;
    }
  }
}

/**
 * Pulls data for a specific entity's text sections field
 *
 * @return
 *   Returns an associative array with the section_type, max_depth, and 
 *   back_to_top values, or FALSE if there are no values.  
 */
function text_sections_retrieve_data($field_name, $entity_id, $revision_id) {
  $result = db_select('text_sections_data', 'd')
    ->fields('d', array('section_type', 'max_depth', 'back_to_top'))
    ->condition('d.field_name', $field_name, '=')
    ->condition('d.entity_id', $entity_id, '=')
    ->condition('d.revision_id', $revision_id, '=')
    ->execute()
    ->fetchAssoc();
  return $result;
}

/**
 * Implements hook_entity_insert().
 */
function text_sections_entity_insert($entity, $type) {
  text_sections_store_data($entity, $type);
}

/**
 * Implements hook_entity_update().
 *
 * We're using this hook to save the field's supplementary data. It MUST be done
 * at this late stage because otherwise we do not have the revision ID yet and
 * we need the right one as the key.
 */
function text_sections_entity_update($entity, $type) {
  text_sections_store_data($entity, $type);
}

/**
 * Callback to extract and store node field values for text sections fields
 */
function text_sections_store_data($entity, $type) {
  // Let's go ahead and get some info about this entity...
  $entity_ids = entity_extract_ids($type, $entity); // 0 is id, 1 is revision id, 2 is bundle
  $language = isset($entity->language) ? $entity->language : LANGUAGE_NONE;

  // Get its instances...
  $instances = field_info_instances($type, $entity_ids[2]);
  
  // And find the fields that are text sections.
  $fields = array();
  $available = array('text_sections_field_widget');
  foreach ($instances as $machine_name => $field_instance) {
    if (in_array($field_instance['widget']['type'], $available))
      $fields[] = $machine_name;
  }

  // With the field names we can loop through the post data to find what we want
  foreach ($fields as $field_name) {
    $section_type = intval($_POST[$field_name][$language]['section_type']);
    $max_depth = intval($_POST[$field_name][$language]['max_depth']);
    $back_to_top = isset($_POST[$field_name][$language]['back_to_top']) ? intval($_POST[$field_name][$language]['back_to_top']) : 0;

    db_merge('text_sections_data')
      ->key(array(
        'field_name' => $field_name, 
        'entity_id' => $entity_ids[0],
        'revision_id' => $entity_ids[1],
      ))
      ->fields(array(
        'section_type' => $section_type,
        'max_depth' => $max_depth,
        'back_to_top' => $back_to_top,
      ))
      ->execute();
  }
}

/**
 * 
 */
function text_sections_field_entity_settings_form($form, &$form_state, $element) {
  // Get the existing data for the grouping
  $entity_ids = entity_extract_ids($element[0]['#entity_type'], $element[0]['#entity']);
  
  // Check for $_POST data. It that is set, we want to use that because the form is reloading
  if (isset($_POST[$element['#field_name']])) {
    $default_data['section_type'] = intval($_POST[$element['#field_name']][$element['#language']]['section_type']);
    $default_data['max_depth'] = intval($_POST[$element['#field_name']][$element['#language']]['max_depth']);
    $default_data['back_to_top'] = isset($_POST[$element['#field_name']][$element['#language']]['back_to_top']) ? intval($_POST[$element['#field_name']][$element['#language']]['back_to_top']) : 0;
  }
  else {
    $default_data = text_sections_retrieve_data($element['#field_name'], $entity_ids[0], $entity_ids[1]);
    if (!$default_data) {
      $default_data = array(
        'section_type' => TEXT_SECTIONS_MANUAL,
        'max_depth' => 0,
        'back_to_top' => 0,
      );
    }
  }

  // Build options
  $section_types = array(
    TEXT_SECTIONS_NONE => 'No Sections',
    TEXT_SECTIONS_MANUAL => 'Manually Input Sections',
    // TEXT_SECTIONS_SEMI => 'Capture Anchors from Text',
    TEXT_SECTIONS_AUTO => 'Automatically Generate Sections',
  );
  $max_depth = array(
    0 => 'View All',
    1 => '1',
    2 => '2',
    3 => '3',
    4 => '4',
    5 => '5',
  );

  $form = array();
  $form['section_type'] = array(
    '#type' => 'select',
    '#title' => t('Text Sections Type'),
    '#options' => $section_types,
    '#default_value' => $default_data['section_type'],
    '#description' => t('Select whether you\'d like sections on this article automatically generated based on header tags or manually generated by creating links and anchor tags in the body field.'),
    '#name' => $element['#field_name'] . '[' . $element['#language'] . '][section_type]',
  );
  $form['max_depth'] = array(
    '#type' => 'select',
    '#title' => t('Text Sections Max Depth'),
    '#default_value' => $default_data['max_depth'],
    '#options' => $max_depth,
    '#description' => t('Select the depth of header tags you would like reflected in the sections. ex. Entering "2" and having the highest header in the article be an &lt;h2&gt; would result in all &lt;h2&gt; and &lt;h3&gt; headers being used for sections.'),
    '#name' => $element['#field_name'] . '[' . $element['#language'] . '][max_depth]',
    '#states' => array(
      'visible' => array(   // action to take.
        ':input[name="' . $element['#field_name'] . '[' . $element['#language'] . '][section_type]"]' => array('value' => TEXT_SECTIONS_AUTO),
      ),
    ),
  );
  $form['back_to_top'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add "Return to Top" In Sections'),
    '#default_value' => $default_data['back_to_top'],
    '#name' => $element['#field_name'] . '[' . $element['#language'] . '][back_to_top]',
    '#states' => array(
      'visible' => array(   // action to take.
        ':input[name="' . $element['#field_name'] . '[' . $element['#language'] . '][section_type]"]' => array(array('value' => TEXT_SECTIONS_AUTO), array('value' => TEXT_SECTIONS_MANUAL)),
      ),
    ),
  );

  return $form;
}

/**
 * Implements hook_preprocess_field().
 */
function text_sections_preprocess_field(&$variables) {
  // Let's start by figuring out if this is a field we need to have an affect on
  // Get all the instances for this entity
  $instances = field_info_instances($variables['element']['#entity_type'], $variables['element']['#bundle']);
  foreach ($instances as $machine_name => $field_instance) {
    if ($field_instance['widget']['type'] == 'text_sections_field_widget' && $field_instance['settings']['attach'] == $variables['element']['#field_name']) {
      // Now we're in business. We just need to check that we're doing automatic ones here.
      $entity_ids = entity_extract_ids($variables['element']['#entity_type'], $variables['element']['#object']);
      $data = text_sections_retrieve_data($machine_name, $entity_ids[0], $entity_ids[1]);
      if ($data && $data['section_type'] == TEXT_SECTIONS_AUTO) {
        // Let's do the thing and add the anchors
        $variables['items'][0]['#markup'] = text_sections_generate_sections($variables['items'][0]['#markup'], 'text', $data['max_depth']);
      }
    }
  }
}

/**
 *
 *
 * @return
 *   An array of section data if the $op is set to 'sections', the edited text
 *   with new anchors if the $op is set to 'text'.
 */
function text_sections_generate_sections($text, $op = 'sections', $max_depth = 6, $start = 0, $name_prefix = 'section-') {
  $max_depth = ($max_depth < 1) ? 6 : $max_depth;
  // Start by finding all the opening header tags. We'll use the capture group to get the depth of the header
  $matches = array();
  preg_match_all('/<h(\d)/', $text, $matches, PREG_OFFSET_CAPTURE);

  // Take just the capture data
  $headers = $matches[1];

  // Get the highest level header.
  $top = array();
  $highest = 6;
  if (count($headers) > 0)
    $top = min($headers);
  if (count($top) > 0)
    $highest = $top[0];

  // Loop through, parse, and exempt headers
  foreach($headers as $key => $value){
    // Removing anything under the max depth
    if($value[0] >= $highest + $max_depth){
      unset($headers[$key]);
    } else {
      // Get the contents of the tag
      $match = array();

      // Grab the rest of the tag. This will break on nested headers of the same level.
      preg_match('/>([\s\S]+?)<\/h' . $value[0] . '>/', $text, $match, 0, $value[1]);

      // Let's clean up the header. In order, we're:
      //  - Replacing multiple whitespace chars (including linebreaks) with a sigle whitespace
      //  - Stripping tags except the most basic markup
      //  - Trimming
      $header_text = trim(strip_tags(preg_replace('/\s+/', ' ', $match[1]), '<em><strong><i><b>'));

      // Drop empty headers
      if ($header_text != '')
        $headers[$key][2] = $header_text;
      else
        unset($headers[$key]);

      // Let's check for our omission class "text-section-skip"
      $class_check = substr($text, $value[1], strpos($text, '>', $value[1]) - $value[1]);
      if(preg_match("/[\s\S]+?class=['\"][\s\S]*?text-section-skip[\s'\"]/", $class_check))
        unset($headers[$key]);
    }
  }
  $headers = array_values($headers);

  switch ($op) {
    case 'sections':
      // Loop through and make the links for the section header
      $sections = array();
      $depth_stack = array();
      $last_depth = $highest; // Fortunately we already figured out what the highest level header is
      $count = -1;
      foreach($headers as $key => $header){
        if ($last_depth < $header[0]) {
          // Handle it if we jump more than one header in depth
          for ($i = 0; $i < $header[0] - $last_depth - 1; $i++) {
            array_push($depth_stack, $count - 1);
          }
          array_push($depth_stack, $count);
          $last_depth = $header[0];
        }
        elseif ($last_depth > $header[0]) {
          // Handle it if we jump more than one header in depth
          for ($i = 0; $i < $last_depth - $header[0]; $i++) {
            array_pop($depth_stack);
          }
          $last_depth = $header[0];
        }
        
        // Set the pid based on the depth stack
        $pid = (count($depth_stack) > 0 && $depth_stack[count($depth_stack) - 1] >= 0) ? $depth_stack[count($depth_stack) - 1] : NULL;
        
        // Create section data
        $sections[] = array(
          'name' => $header[2],
          'anchor' => $name_prefix . $key,
          'id' => $count + 1,
          'pid' => $pid,
        );
        $count++;
      }
      return $sections;
    case 'text':
      // Loop through backwards so the positions stay correct and make the anchors
      krsort($headers);
      foreach($headers as $key => $header){
        $text = substr_replace($text, '<a name="' . $name_prefix . $key . '" class="text-sections-auto"></a>', $header[1] - 2, 0);
      }
      return $text;
  }
  
  return FALSE;
}